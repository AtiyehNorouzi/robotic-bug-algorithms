Implementation of Bug0 , Bug1, Bug2 Algorithms with C# using Unity Game Engine.

**Bug0 Incompleteness:**

![bug0_incompleteness](/uploads/14a401278d8226a4d2ef4ddf3d7bdab4/bug0_incompleteness.PNG)

**Compare algorithms Performance in a simple scenario:**

Bug0:


![bug0](/uploads/937f5b84bc7327ff4a6a70dc417c3897/bug0.PNG)

Bug1:

![bug1](/uploads/092025ac3d96892b1a963cd748b4068f/bug1.PNG)

Bug2:

![bug2](/uploads/3b84d41c3511d1366387cfb2c45a7b9c/bug2.PNG)

**Optional:**

There is an option to add some dynamic lights to your project in Unity Editor(by enabling 2DLight gameObject)

![dynamic_light](/uploads/03e18926d87e15701e06e4cd60e17811/dynamic_light.PNG)