﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveObjsByMouse : MonoBehaviour
{
    #region FIELDS
    Vector3 _screenPoint;
    #endregion

    #region MONO BEHAVIOURS
    void OnMouseDown()
    {
        _screenPoint = Camera.main.WorldToScreenPoint(Input.mousePosition);
    }

    void OnMouseDrag()
    {
        Vector3 curScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, transform.position.z);
        Vector3 curPosition = Camera.main.ScreenToWorldPoint(curScreenPoint) ;
        transform.position = new Vector3(curPosition.x , curPosition.y , transform.position.z);
    }
    #endregion
}
