﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathFindingUtility : MonoBehaviour
{

    public static Vector3 CalculateRotation(CharacterMove character, Vector3 dir) // calculate rotation direction with right and left raycast points
    {
        Vector3 _frontPoint = GetRayCastPoint(character.LeftHand.transform.position, dir, 2f);
        Vector3 _backPoint = GetRayCastPoint(character.RightHand.transform.position, dir, 2f);
        Vector3 _dir = CalculateDirection(_frontPoint, _backPoint);
        _dir = Mathf.Abs(_dir.x) >= 0.5 ? Vector3.right : Vector3.up;
        ChangeRotation(_dir , character.gameObject);
        return _dir;
    }

    public static Vector3 GetRayCastPoint(Vector3 origin, Vector3 dir, float dist)
    {
        RaycastHit2D _rch;
        _rch = Physics2D.Raycast(origin,
          dir, dist, LayerMask.GetMask("Default"));
        return _rch.point;
    }

    public static Vector3 NormalizedDir(Vector3 dir)
    {
        if (Mathf.Abs(dir.x) >= 0.5)
            return Vector3.right * Mathf.Sign(dir.x);
        else if (Mathf.Abs(dir.y) >= 0.5)
            return Vector3.up * Mathf.Sign(dir.y);
        return Vector3.up;
    }

    public static void ChangeRotation(Vector3 dir , GameObject _character)
    {
        var angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        _character.transform.rotation = Quaternion.AngleAxis(angle - 90, Vector3.forward);
    }

    public static Vector3 CalculateDirection(Vector3 origin, Vector3 target)
    {
        return new Vector3((target.x - origin.x), (target.y - origin.y), 0).normalized;
    }

    public static bool CheckCanChangeDirection(CharacterMove _character , Vector3 raycastDir, float dist)
    {
        if (LineRayCast(_character.LeftHand.transform.position, raycastDir, dist)
           || LineRayCast(_character.RightHand.transform.position, raycastDir, dist)
           || LineRayCast(_character.transform.position, raycastDir, dist))
            return false;
        return true;
    }

    public static bool LineRayCast(Vector3 origin, Vector3 dir, float dist)
    {
        RaycastHit2D _rch;
        _rch = Physics2D.Raycast(origin, dir, dist, LayerMask.GetMask("Default"));
        if (_rch.collider != null)
            return true;
        return false;
    }

    public static float GetDistance(Vector3 a, Vector3 b)
    {
        return new Vector3(a.x - b.x, a.y - b.y, 0).magnitude;
    }

    public static Vector3 FindNearestDir(List<Vector3> list , Vector3 origin , Vector3 target)
    {
        Vector3 prev = PathFindingUtility.NormalizedDir(list[list.Count - 1]);
        if (list.Count == 2 || list.Count == 3)
            return -list[list.Count - 2];

        if (prev.x == 0)
            if (origin.x <= target.x)
                return Vector3.right;
            else
                return Vector3.left;
        else if (prev.y == 0)
            if (origin.y <= target.y)
                return Vector3.up;
            else
                return Vector3.down;
        return Vector3.right;

    }
}
