﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bug2PathFinding : MonoBehaviour , IBugPathFinding
{
    #region FIELDS
    private List<Vector3> _dirMemory = new List<Vector3>();
    private Vector3 _targetPos;
    private CharacterMove _character;
    private Vector3 _raycastDir;
    private Vector3 _moveDir;
    MoveState _currentState = MoveState.HeadGoal;
    private Vector3 _startPoint;
    #endregion

    #region ENUMS
    enum MoveState
    {
        HeadGoal = 0, FollowWall
    }
    #endregion

    #region MONO BEHAVIOUR
    public IEnumerator HeadToGoal()
    {
        while (true)
        {
            if (_currentState == MoveState.HeadGoal)
            {
                if (PathFindingUtility.GetDistance(_character.transform.position, _targetPos) <= 0.5f)// check if character reached target
                    yield break;

                if (!HeadTowardGoal())//if character hits an obstacle , it rounds around obstacle to find nearest point to target
                {
                    _currentState = MoveState.FollowWall;
                    _moveDir = PathFindingUtility.CalculateRotation(_character, _raycastDir);
                    _dirMemory.Add(_moveDir);
                }
            }
            yield return null;
        }
    }
    public IEnumerator FollowWall()
    {
        while (true)
        {
            if (PathFindingUtility.GetDistance(_character.transform.position, _targetPos) <= 0.5f)// check if character reached target
                yield break;

            if (_currentState == MoveState.FollowWall)
            {          
                    if (IsOnDirection(_character.transform.position , _startPoint) && _dirMemory.Count >= 2)
                    {
                        _currentState = MoveState.HeadGoal;
                        _dirMemory.Clear();
                    }
            }
            _character.transform.Translate(Vector3.up * 5f * Time.deltaTime);
            yield return null;
        }
    }

    public IEnumerator CheckIfObstacleEnds()
    {
        while (true)
        {
            if (_currentState == MoveState.FollowWall)
            {
                if (PathFindingUtility.CheckCanChangeDirection(_character, _raycastDir, 5f))
                {
                    _raycastDir = -_dirMemory[_dirMemory.Count - 1];
                    Vector3 direction = PathFindingUtility.FindNearestDir(_dirMemory , _character.transform.position , _targetPos);
                    _dirMemory.Add(direction);
                    PathFindingUtility.ChangeRotation(direction, _character.gameObject);
                }
            }
            yield return new WaitForSeconds(0.1f);
        }
    }
    #endregion

    #region PUBLIC METHODS
    public void FindPath(CharacterMove character)
    {
        this._character = character;
        StartCoroutine(HeadToGoal());
        StartCoroutine(FollowWall());
        StartCoroutine(CheckIfObstacleEnds());
    }

    public bool HeadTowardGoal()
    {
        Vector3 direction = PathFindingUtility.CalculateDirection(_character.transform.position, _targetPos);
        RaycastHit2D raycasthit = Physics2D.Raycast(_character.transform.position, direction, 1f, LayerMask.GetMask("Default"));
        if (raycasthit.collider == null)
        {
            PathFindingUtility.ChangeRotation(direction, _character.gameObject);
            return true;
        }
        _startPoint = _character.transform.position;
        _raycastDir = PathFindingUtility.NormalizedDir(PathFindingUtility.CalculateDirection(_character.transform.position, raycasthit.point));
        return false;
    }

    public void SetTarget(Vector3 targetPos)
    {
        _targetPos = targetPos;
    }
    #endregion

    #region PRIVATE METHODS
    private bool IsOnDirection(Vector3 point, Vector3 startpoint)
    {
        if (Mathf.Abs(point.x / startpoint.x - point.y / startpoint.y) <= 0.2f)
            return true;
        return false;
    }
    #endregion
}
