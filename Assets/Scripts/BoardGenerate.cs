﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardGenerate : MonoBehaviour
{
    #region FIELDS
    [SerializeField]
    private GameObject[] _boxes;
    #endregion

    #region MONOBEHAVIOURS
    private void Start()
    {
        foreach (GameObject box in _boxes)
        {
            if (Random.value < 0.3f)
                box.SetActive(false);
        }
    }
    #endregion
}
