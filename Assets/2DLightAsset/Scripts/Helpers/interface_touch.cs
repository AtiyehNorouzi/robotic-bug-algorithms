﻿namespace DynamicLight2D
{
	using UnityEngine;
	using System.Collections;
	
	public class interface_touch: MonoBehaviour {
		
		GameObject cLight;
		GameObject cubeL;
         Transform character;
		//GUIText UIlights;
		//GUIText UIvertex;
		
		
		[HideInInspector] public static int vertexCount;
		
		//int lightCount = 1;
		
		
		void Start () {

            character = GameObject.FindObjectOfType<CharacterMove>().transform;
			cLight = GameObject.Find("2DLight");
			
			StartCoroutine (LoopUpdate ());
			
		}
		
		// Update is called once per frame
		IEnumerator LoopUpdate () {
			
			while (true) {
				Vector3 pos = character.position;
				yield return new WaitForEndOfFrame ();
				cLight.transform.position = pos;
				
			}
			
			
		}
		
		
		
	}

}